<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/order/create', 'OrderController@create')
    ->middleware('\Barryvdh\Cors\HandleCors::class');

Route::post('/driver/orders', 'DriverController@getOrders')
    ->middleware('\Barryvdh\Cors\HandleCors::class');