<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email'); // el email puede ser distinto al asociado con el cliente
            $table->string('phone', 30); // el teléfono puede ser distinto al asociado con el cliente
            $table->string('delivery_address');
            $table->dateTime('delivery_time_start');
            $table->dateTime('delivery_time_end');
            $table->unsignedInteger('created_by')->unsigned();
            $table->unsignedInteger('assigned_to')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('clients');
            $table->foreign('assigned_to')->references('id')->on('drivers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
