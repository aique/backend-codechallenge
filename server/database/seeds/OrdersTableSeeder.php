<?php

use Illuminate\Database\Seeder;
use App\Order;

class OrdersTableSeeder extends Seeder
{
    const DATE_FORMAT = 'Y-m-d H:i:s';

    public function run() {
        Order::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0 ; $i < 10 ; $i++) {
            $createdBy = rand(1, 10);
            $assignedTo = 1;

            $this->createOrder($faker, $createdBy, $assignedTo);
        }

        for ($i = 0 ; $i < 5 ; $i++) {
            $createdBy = rand(1, 10);
            $assignedTo = 2;

            $this->createOrder($faker, $createdBy, $assignedTo);
        }
    }

    private function createOrder($faker, $createdBy, $assignedTo) {
        $deliveryTimeStart = $faker->dateTimeThisMonth();
        $deliveryTimeEnd = $this->getDeliveryTimeEnd($deliveryTimeStart);

        Order::create([
            'email' => $faker->email,
            'phone' => $faker->phoneNumber,
            'delivery_address' => $faker->address,
            'delivery_time_start' => $deliveryTimeStart,
            'delivery_time_end' => $deliveryTimeEnd,
            'created_by' => $createdBy,
            'assigned_to' => $assignedTo,
        ]);
    }

    private function getDeliveryTimeEnd($deliveryTimeStart) {
        $deliveryTimeEnd = DateTime::createFromFormat(SELF::DATE_FORMAT, $deliveryTimeStart->format(self::DATE_FORMAT));
        $numHoursInTimeRange = rand(1, 8);
        $interval = new DateInterval('PT'.$numHoursInTimeRange.'H');
        $deliveryTimeEnd->add($interval);

        return $deliveryTimeEnd;
    }
}
