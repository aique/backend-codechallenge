<?php

use Illuminate\Database\Seeder;
use App\Client;

class ClientsTableSeeder extends Seeder
{
    public function run() {
        $faker = \Faker\Factory::create();

        for ($i = 0 ; $i < 9 ; $i++) {
            $email = $faker->email;

            Client::create([
                'name' => $faker->firstName,
                'surname' => $faker->lastName,
                'email' => $email,
                'phone' => $faker->phoneNumber,
                'password' => bcrypt($email)
            ]);
        }

        $defaultClientEmail = 'client@mail.com';

        Client::create([
            'name' => $faker->firstName,
            'surname' => $faker->lastName,
            'email' => $defaultClientEmail,
            'phone' => $faker->phoneNumber,
            'password' => bcrypt($defaultClientEmail)
        ]);
    }
}
