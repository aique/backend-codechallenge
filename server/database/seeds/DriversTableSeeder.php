<?php

use Illuminate\Database\Seeder;
use App\Driver;

class DriversTableSeeder extends Seeder
{
    public function run() {
        $faker = \Faker\Factory::create();

        for ($i = 0 ; $i < 1 ; $i++) {
            $email = $faker->email;

            Driver::create([
                'name' => $faker->firstName,
                'surname' => $faker->lastName,
                'email' => $email,
                'phone' => $faker->phoneNumber,
                'password' => bcrypt($email)
            ]);
        }

        $defaultDriverEmail = 'driver@mail.com';

        Driver::create([
            'id' => 2,
            'name' => $faker->firstName,
            'surname' => $faker->lastName,
            'email' => $defaultDriverEmail,
            'phone' => $faker->phoneNumber,
            'password' => bcrypt($defaultDriverEmail)
        ]);
    }
}
