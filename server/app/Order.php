<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model implements \JsonSerializable
{
    protected $hidden = [
        'created_by', 'assigned_to', 'created_at', 'updated_at',
    ];
}
