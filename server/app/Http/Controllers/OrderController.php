<?php

namespace App\Http\Controllers;

use App\Client;
use App\Driver;
use App\Order;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends BaseController
{
    public function create() {
        if ($this->invalidCreateOrderRequest()) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        if (!$this->validCredentials()) {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }

        $order = $this->createOrderFromRequest();
        $order->save();

        return new JsonResponse();
    }

    private function invalidCreateOrderRequest() {
        $rules = array(
            'email' => 'required|email',
            'phone' => 'required|numeric|digits_between:6,30',
            'delivery_address' => 'required',
            'delivery_time_start' => 'required|date',
            'delivery_time_end' => 'required|date',
            'username' => 'required',
            'password' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        return $validator->fails();
    }

    private function createOrderFromRequest() {
        $clientId = $this->getAuthId();
        $assignedDriver = $this->assignToDriver();

        $order = new Order();

        $order->email = Input::get('email');
        $order->phone = Input::get('phone');
        $order->delivery_address = Input::get('delivery_address');
        $order->delivery_time_start = Input::get('delivery_time_start');
        $order->delivery_time_end = Input::get('delivery_time_end');
        $order->created_by = $clientId;
        $order->assigned_to = $assignedDriver;

        return $order;
    }

    private function assignToDriver() {
        $lessBusyDriver = $this->getLessBusyDriver();

        if ($lessBusyDriver) {
            $this->crearDriverCache($lessBusyDriver->id);

            return $lessBusyDriver->id;
        }

        return null;
    }

    private function crearDriverCache($driverId) {
        Cache::forget('orders_driver_'.$driverId);
    }

    /**
     * La estrategia para seleccionar al conductor seguida es aquel que
     * tenga menos pedidos asignados. Es una solución que debería ser
     * adaptada en una aplicación real, por ejemplo teniendo en cuenta
     * no sólo la carga de trabajo sino también si hay entregas dentro
     * de la misma franja horaria.
     */
    private function getLessBusyDriver() {
        $noOrdersDriver = Driver::leftJoin('orders', 'drivers.id', '=', 'orders.assigned_to')
            ->where('orders.assigned_to', null)
            ->first();

        if ($noOrdersDriver) {
            return $noOrdersDriver;
        }

        return Driver::select(DB::raw('drivers.*, count(*) as `aggregate`'))
            ->join('orders', 'drivers.id', '=', 'orders.assigned_to')
            ->groupBy('drivers.id')
            ->orderBy('aggregate', 'asc')
            ->take(1)
            ->first();
    }

    private function getAuthId() {
        $credentials = $this->createCredentialsFromRequest();
        $client = Client::where('email', $credentials['email'])->first();

        return $client->id;
    }

    private function createCredentialsFromRequest() {
        return [
            'email' => Input::get('username'),
            'password' => Input::get('password')
        ];
    }

    private function validCredentials() {
        $credentials = $this->createCredentialsFromRequest();

        return Auth::guard('clients')->attempt($credentials);
    }
}