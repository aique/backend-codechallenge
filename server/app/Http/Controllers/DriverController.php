<?php

namespace App\Http\Controllers;

use App\Client;
use App\Driver;
use App\Order;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DriverController extends BaseController
{
    public function getOrders() {
        if (!$this->validCredentials()) {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }

        $orders = $this->getOrdersFromCache();

        return new JsonResponse($orders);
    }

    private function validCredentials() {
        $credentials = $this->createCredentialsFromRequest();

        return Auth::guard('drivers')->attempt($credentials);
    }

    private function getOrdersFromCache() {
        $credentials = $this->createCredentialsFromRequest();
        $id = $this->getAuthId($credentials);

        return Cache::rememberForever('orders_driver_'.$id, function() use($id) {
            return Order::where('assigned_to', $id)
                ->orderBy('delivery_time_end', 'ASC')
                ->get();
        });
    }

    private function createCredentialsFromRequest() {
        return [
            'email' => Input::get('username'),
            'password' => Input::get('password')
        ];
    }

    private function getAuthId($credentials) {
        $client = Driver::where('email', $credentials['email'])->first();

        return $client->id;
    }
}