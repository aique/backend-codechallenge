<?php

return [
    'supportsCredentials' => false,
    'allowedOrigins' => ['https://prueba-goi-client.com'],
    'allowedHeaders' => ['Content-Type', 'X-Requested-With'],
    'allowedMethods' => ['GET'],
    'exposedHeaders' => [],
    'maxAge' => 0,
];