<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/common.css">

    <title>GOi Test Client</title>

</head>
<body>

    <?php include "common/header.php" ?>

    <div id="section-content" class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1>Crear pedido</h1>

                <div class="row">

                    <div id="alert-layout" class="col-12 col-md-8 offset-md-2"></div>

                    <form id="create-order-form" class="col-12 col-md-8 offset-md-2">

                        <div class="form-group row">
                            <label for="example-text-input" class="col-4 col-form-label">Email de contacto</label>
                            <div class="col-8">
                                <input class="form-control" type="email" name="email" required="required">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-4 col-form-label">Teléfono de contacto</label>
                            <div class="col-8">
                                <input class="form-control" type="number" name="phone" required="required">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-4 col-form-label">Dirección de entrega</label>
                            <div class="col-8">
                                <input class="form-control" type="text" name="delivery_address" required="required">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-4 col-form-label">Fecha de entrega (desde)</label>
                            <div class="col-8">
                                <input class="form-control" type="datetime-local" name="delivery_time_start" required="required">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-4 col-form-label">Fecha de entrega (hasta)</label>
                            <div class="col-8">
                                <input class="form-control" type="datetime-local" name="delivery_time_end" required="required">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary float-right">Crear</button>

                    </form>

                </div>

            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <!-- Variables globales -->

    <script>
        var _config;
    </script>

    <script src="js/views/common.js"></script>
    <script src="js/views/create.js"></script>
    <script src="js/moment.min.js"></script>

</body>
</html>