$(function() {
    parseConfigFile(null);
    createOrderOnFormSubmit();
});

function createOrderOnFormSubmit() {
    $('#create-order-form').on('submit', function(ev) {
        ev.preventDefault();
        createOrder($(this).serialize());
    });
}

function createOrder(formData) {
    var data = addCredentials(formData);

    $.ajax({
        method: 'POST',
        url: _config.create.url,
        data: data
    })
    .done(function() {
        showFormAlert('success', 'Pedido creado con éxito');
    })
    .fail(function() {
        showFormAlert('danger', 'Servicio no disponible');
    });
}

function addCredentials(data) {
    return data +
        '&username=' + _config.create.credentials.username +
        '&password=' + _config.create.credentials.password;
}

function showAlert(alertClass, msg) {
    printAlertInLayout(alertClass, msg);
    $('#create-order-form').hide();
}

function showFormAlert(alertClass, msg) {
    printAlertInLayout(alertClass, msg);
    $('.alert').css('display', 'block');
}

function printAlertInLayout(alertClass, msg) {
    $('#alert-layout')
        .html('')
        .append($('<p></p>')
            .addClass('alert alert-' + alertClass)
            .html(msg));
}