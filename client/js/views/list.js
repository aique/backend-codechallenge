var _config;

$(function() {
    showOrdersList();
});

function showOrdersList() {
    parseConfigFile(queryOrdersList);
}

function queryOrdersList() {
    $.ajax({
        method: 'POST',
        url: _config.list.url,
        data: {
            username: _config.list.credentials.username,
            password: _config.list.credentials.password,
        }
    })
    .done(function(orders) {
        printOrders(orders)
    })
    .fail(function() {
        showAlert('danger', 'Servicio no disponible');
    });
}

function printOrders(orders) {
    if (orders.length > 0) {
        printOrdersTable(orders);
    } else {
        showAlert('warning', 'No tienes pedidos asignados');
    }
}

function printOrdersTable(orders) {
    var responseLayout = getEmptyLayout();
    responseLayout.append(getOrdersTable(orders));
}

function getOrdersTable(orders) {
    var table = ($('<table></table>').addClass('table'));

    table.append(createOrderTableHeader());
    table.append(createOrderTableBody(orders));

    return table;
}

function createOrderTableHeader() {
    return $('<thead></thead>')
            .append($('<tr></tr>')
                .append($('<th></th>').html('Fecha de entrega'))
                .append($('<th></th>').html('Id'))
                .append($('<th></th>').html('Email'))
                .append($('<th></th>').html('Teléfono'))
                .append($('<th></th>').html('Dirección'))
            );
}

function createOrderTableBody(orders) {
    var tbody = $('<tbody></tbody>');

    for (var i = 0 ; i < orders.length ; i++) {
        tbody.append(createOrderRow(orders[i]));
    }

    return tbody;
}

function createOrderRow(order) {
    return $('<tr></tr>')
        .append($('<td></td>').html(getDeliveryTime(order)))
        .append($('<td></td>').html(order.id))
        .append($('<td></td>').html(order.email))
        .append($('<td></td>').html(order.phone))
        .append($('<td></td>').html(order.delivery_address));
}

function getDeliveryTime(order) {
    var formattedStart = moment(order.delivery_time_start).format('MMMM Do, HH:mm:ss');
    var formattedEnd = moment(order.delivery_time_end).format('HH:mm:ss');

    return formattedStart + ' - ' + formattedEnd;
}

function showAlert(alertClass, msg) {
    getEmptyLayout().append($('<p></p>')
        .addClass('alert alert-' + alertClass)
        .html(msg));
}

function getEmptyLayout() {
    var responseLayout = $('#orders-list');
    responseLayout.html('');

    return responseLayout;
}