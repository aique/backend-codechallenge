function parseConfigFile(callback) {
    $.getJSON('conf/config.json', function(configJsonData) {
        _config = configJsonData;

        if (callback != null) {
            callback();
        }
    })
    .fail(function() {
        showAlert('danger', 'Fichero de configuración incorrecto, sigue las instrucciones del README.');
    });
}