# Code Challenge - Backend

# Instalación

## Servidor

Dentro de la carpeta del proyecto, será necesario ejecutar la instrucción `composer install` en el directorio `server`.

Será necesario crear una base de datos y/o adaptar el fichero de configuración para poder acceder a ella.

Hay que crear un dominio en el fichero `/etc/hosts` para el servidor. Para esta prueba he añadido lo siguiente:

```
127.0.0.1       prueba-goi-server.com
```

Una vez hecho esto, hay que crear un host virtual en apache mediante los siguientes pasos:

- Creamos el fichero de configuración en el directorio `/etc/apache2/sites-available`.
- Generamos los certificados necesario para utilizar SSL, ya que desde el cliente se enviarán los datos de usuario y esta operación ha de realizarse a través de un canal seguro:
    - Mediante el comando `sudo mkdir /etc/apache2/ssl` se creará el directorio que contendrá los certificados.
    - Los certificados se generarán con la instrucción `sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.crt`.
- Editamos el fichero con el siguiente contenido (realizar las modificaciones pertinentes):

```
<IfModule mod_ssl.c>
    <VirtualHost _default_:443>
        ServerName prueba-goi-server.com
        DocumentRoot {project-path}/server

        <Directory {project-path}/server>
            Options FollowSymLinks Includes
            AllowOverride All
            Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/prueba-goi-server-error.log
        CustomLog ${APACHE_LOG_DIR}/prueba-goi-server-access.log combined
        SSLEngine on
        SSLCertificateFile /etc/apache2/ssl/apache.crt
        SSLCertificateKeyFile /etc/apache2/ssl/apache.key
        <FilesMatch "\.(cgi|shtml|phtml|php)$">
                        SSLOptions +StdEnvVars
        </FilesMatch>
        <Directory /usr/lib/cgi-bin>
                        SSLOptions +StdEnvVars
        </Directory>
        BrowserMatch "MSIE [2-6]" \
                        nokeepalive ssl-unclean-shutdown \
                        downgrade-1.0 force-response-1.0
        BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
    </VirtualHost>
</IfModule>
```

Ejecutar las siguientes ordenes para crear las tablas y rellenarlas con los datos iniciales:

```
php artisan migrate
php artisan db:seed
```

## Cliente

Será necesario crear un dominio y un host virtual de manera similar a cómo se ha hecho con el servidor.

Además de esto, habrá que copiar el fichero 'client/conf/example.config.json' en el mismo directorio, con el nombre `config.json`.
En él están incluidas las rutas por defecto de los endpoints y las credenciales tanto para el cliente como para el conductor.
Por defecto, se podrá ejecutar la prueba simplemente con la configuración por defecto.

# Ejecución de la prueba

Basta con acceder mediante navegador a la ruta del cliente.

Vista del formulario de alta de un envío:

![Formulario de alta de un envío](https://gitlab.com/aique/backend-codechallenge/raw/master/screenshots/goi-1.png)

Vista de listado de envíos asignados a un conductor:

![Listado de envíos asignados a un conductor](https://gitlab.com/aique/backend-codechallenge/raw/master/screenshots/goi-2.png)

# Aclaraciones

## Base de datos

Se han creado dos tablas de usuarios, además de la proporcionada por defecto. Estas dos tablas son las de clientes y conductores.
Si bien para este ejemplo tienen la misma información y son redundantes (un campo con el rol cliente/conductor podría bastar),
lo normal es que la información almacenada en cada una de ellas difiera en una aplicación real.

Por otra parte la tabla de usuarios por defecto no se ha utilizado, simulando los usuarios que podrían acceder de manera interna
al servidor como trabajadores de la empresa (por ejemplo los administradores).

## Autenticación

Mi intención era utilizar oAuth2 para la autenticación de las llamadas a la API. Con la estrategia `password grant` comprobaría
desde el cliente si el usuario tiene un token. De ser así realizaría la llamada a la API con él, y en caso contrario mostraría
un formulario de login para poder obtenerlo.

No obstante, parece que `laravel\passport` no permite el multiauth de usuarios, y utiliza por defecto el `guard` predefinido
de usuarios para realizar el login (más info en https://github.com/laravel/passport/issues/161).

La solución pasaba por modificar la librería una vez descargada con `composer`, con lo que para que la instalación de la
prueba por vuestra parte fuera más simple, he optado por pasar el usuario y la constraseña como parte de la petición (soy consciente
de que no es correcto, pero en el ámbito de una pequeña prueba para comprobar las aptitudes del candidato, me parece suficiente).
Por supuesto esta información se intercambia sobre protocolo `https`.

# Enunciado

Imaginemos que un cliente solicita el envío de un pedido mediante una llamada a la API REST para almacenarlo en la base de datos.

El pedido debe contener:

- Nombre y apellidos del cliente
- Email (Único por cliente)
- Teléfono
- Dirección de entrega (solo puede existir una por pedido)
- Fecha de entrega
- Franja de hora seleccionada para la entrega (variable, pueden ser desde franjas de 1h hasta de 8h)

Una vez tenemos guardada la información del pedido, debe asignarse a un driver que tengamos dado de alta en el sistema de forma aleatoria.

Por otro lado, nuestros drivers mediante su aplicación, necesitan obtener el listado de tareas para completar en el día. Es necesario contar con un endpoint que reciba como parámetro el ID del driver y la fecha de los pedidos que queremos obtener y nos devuelva un JSON con el listado.

# TODO
- Arquitectura de aplicación en Laravel
- Construir el modelo de datos en MYSQL con todas las entidades y relaciones
- Endpoint para persistir el pedido en BD
- Endpoint para mostrar los pedidos a entregar por los drivers

# Evaluable
- Diseño modelado de datos
- API REST con sus endpoints
- Arquitectura de aplicación en Laravel
- Utilización del ORM
- Uso de buenas prácticas
- Patrones de diseño utilizados
- Optimización del performance

# Workflow
- Haz un fork de este repositorio.
- Resuelve el ejercicio.
- Comparte tu fork para la corrección (Reporter access)

Si tienes alguna duda, puedes contactar con nosotros en `tech@letsgoi.com`

Muchas gracias y suerte.